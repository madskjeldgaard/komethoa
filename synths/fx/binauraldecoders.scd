(
[\binauralListenDecoder, \binauralCIPICDecoder].do{|decodername|
var desc = "A % based binaural decoder".format(decodername),
func = {|order|
    {|hoaIn, yaw=0, pitch=0, roll=0|

            // @TODO this should actually be freed after usage, but as it's persistent until recompile I guess it doesn't matter too much
            var binauralDecoder = Komet.perform(decodername);

            // HOA input
            var numChans = order.asHoaOrder.size;
            var clean = hoaIn.keep(numChans);
            var hoa = clean;

            // format exchange: HOA >> FOA
            var lowCutFreq = 30.0;  // highpass freq

            // design encoder to exchange (ordering, normalisation)
            var encoder = FoaEncoderMatrix.newHoa1;
            var foa, stereo, sig;

            // Rotate scene
            hoa = HoaYPR.ar(in: hoa,  yaw: yaw * pi,  pitch: pitch * pi,  roll: roll * pi,  order: order);

            // exchange (ordering, normalisation)
            // truncate to HOA1
            foa = FoaEncode.ar(hoa.keep(AtkFoa.defaultOrder.asHoaOrder.size), encoder);

            // pre-condition FOA to make it work with FoaProximity
            foa = HPF.ar(foa, lowCutFreq);

            // Exchange reference radius
            foa = FoaProximity.ar(foa, AtkHoa.refRadius);

            // Decode to binaural
            stereo = FoaDecode.ar(in: foa,  decoder: binauralDecoder);

            // Pad output with silence after the stereo channels
            stereo = stereo ++ Silent.ar().dup(numChans-2);

            stereo;
        }
    },
    type = \fx,
    category = \hoa,
    basename = decodername.asString.toLower().asSymbol,
    numChans = nil, // TODO: Not used
    specs = (
        bypass: [0, 1, \lin, 0].asSpec,
        yaw: [-1.0, 1.0, \lin, 0].asSpec,
        pitch: [-1.0, 1.0, \lin, 0].asSpec,
        roll: [-1.0, 1.0, \lin, 0].asSpec,
    );

    KometSynthFuncDef(
        basename,
        func,
        type,
        category,
        numChans,
        desc,
        specs,
    ).add();
}
)
