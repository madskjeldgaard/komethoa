// TODO: *Not sure this works correctly yet
(
var desc = "Convert from Ambix to ATK's HOA format format",
func = {|order|
    {|hoaIn|
        var hoa = hoaIn;

        hoa = HoaNFCtrl.ar(
            in: hoa,
            encRadius: 10.0,
            decRadius: AtkHoa.refRadius,
            order: order
        );

        // exchange normalisation: SN3D to N3D
        hoa = HoaDecodeMatrix.ar(
            in: hoa,
            hoaMatrix: HoaMatrixDecoder.newFormat(format: \atk, order: order)
        );

        hoa;

    }
},
type = \fx,
category = \hoa,
basename = \ambix2atk,
numChans = nil, // TODO: Not used
specs = ();

KometSynthFuncDef(
    basename,
    func,
    type,
    category,
    numChans,
    desc,
    specs,
).add();
)
