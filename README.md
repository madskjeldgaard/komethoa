# komethoa

## HOA Extensions for the Komet system

This package adds High order ambisonics synths and extensions to the [Komet Computermusic system](https://codeberg.org/madskjeldgaard/komet). 

Among other things, it adds a range of ambisonics native effects and synthesizers and also contains a factory process that will auto-generate ambisonics native effect processing synths from all the predefined fx synths of type `\channelized` (ie. mono fx synths spread across multiple channels) using the spherical decomposition technique.

## Installation

Open up SuperCollider and evaluate the following line of code:
`Quarks.install("https://codeberg.org/madskjeldgaard/komethoa")`
