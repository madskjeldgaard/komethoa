KometHOA : KometSynthLibExt{
    *initClass{
        Class.initClassTree(KometSynthLib);
        if((this.class != KometSynthLibExt) && (this.class != Meta_KometSynthLibExt), {
            "Detected Kometsynthlib extension %".format(this.name).postln;
        });

        StartUp.add({
            KometSynthLib.files[\synths] = KometSynthLib.files[\synths] ++ this.synths().files;
            KometSynthLib.files[\fx] = KometSynthLib.files[\fx] ++ this.fx().files;
            KometSynthLib.files[\parfx] = KometSynthLib.files[\parfx] ++ this.parfx().folders.collect{|dir| dir.files}.flatten;
            KometSynthLib.files[\faust] = KometSynthLib.files[\faust] ++ this.faust().folders.collect{|dir| dir.files}.flatten;
        })
    }

    *postInit{
        this.prGenerateHOAFX()
    }

    *prGenerateHOAFX{
        var synthfuncdefs = KometSynthFuncDef.allOfCategory('channelized');

        synthfuncdefs.do{|ksfd|
            var basename = ("hoa" ++ ksfd.name.asString).asSymbol;
            var type = 'fx';
            var category = 'hoa';
            var numChans = nil;
            var desc = ksfd.text;
            var specs = ksfd.specs;
            var func = ksfd.func;

            func = {|order, optimization=\spreadE|
                var beamShape=\basic;
                HOAFX_BASE.createMatrices(order, optimization, beamShape);

                {|hoaIn, radius=1.5|

                    // Convert from B format to A format
                    var aformat = HOAFX_BASE.decode(hoaIn);

                    // Perform fx processing
                    aformat = SynthDef.wrap(
                        ksfd.func.value(HOAFX_BASE.numAFormatChannels),
                        prependArgs: [aformat]
                    );

                    // Convert from A format to B format
                    HOAFX_BASE.encode(aformat);

                }
            };

            KometSynthFuncDef(
                basename,
                func,
                type,
                category,
                numChans,
                desc,
                specs,
            ).add();

        }
    }
}
